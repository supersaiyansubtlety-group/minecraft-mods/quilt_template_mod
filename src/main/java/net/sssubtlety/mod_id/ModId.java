package net.sssubtlety.mod_id;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ModId {
	public static final String NAMESPACE = "mod_id";
	public static final Util.TranslatableString NAME = new Util.TranslatableString("text." + NAMESPACE + ".name");
	public static final Logger LOGGER = LoggerFactory.getLogger(NAME.get());

}
