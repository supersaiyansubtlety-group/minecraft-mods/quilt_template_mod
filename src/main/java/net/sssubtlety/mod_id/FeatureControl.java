package net.sssubtlety.mod_id;

import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.ConfigHolder;
import me.shedaniel.autoconfig.serializer.GsonConfigSerializer;
import net.minecraft.util.ActionResult;
import org.jetbrains.annotations.Nullable;

import static net.sssubtlety.mod_id.ModId.LOGGER;
import static net.sssubtlety.mod_id.Util.isModLoaded;

public class FeatureControl {
    private static final @Nullable Config CONFIG_INSTANCE;
    private static boolean verifySave = true;

    private static ActionResult onConfigSave(ConfigHolder<Config> holder, Config config) {
        if (verifySave && invalidateConfig("Error saving config", holder, config)) {
            verifySave = false;
            holder.save();
            verifySave = true;
            return ActionResult.FAIL;
        }

        return ActionResult.CONSUME;
    }

    private static ActionResult onConfigLoad(ConfigHolder<Config> holder, Config config) {
        if (invalidateConfig("Error loading config", holder, config)) holder.save();
        return ActionResult.CONSUME;
    }

    private static boolean invalidateConfig(String errPrefix, ConfigHolder<Config> holder, Config config) {
        final boolean invalid = false;
        if (invalid) LOGGER.error(errPrefix + ": " + "error message");
        return invalid;
    }

    static {
        if (isModLoaded("cloth-config", ">=6.1.48")) {
            final ConfigHolder<Config> holder = AutoConfig.register(Config.class, GsonConfigSerializer::new);
            CONFIG_INSTANCE = holder.getConfig();
            holder.registerLoadListener(FeatureControl::onConfigLoad);
            holder.registerSaveListener(FeatureControl::onConfigSave);
        } else CONFIG_INSTANCE = null;
    }

    public interface Defaults {
        boolean fetch_translation_updates = true;
    }

    public static boolean shouldFetchTranslationUpdates() {
        return CONFIG_INSTANCE == null ? Defaults.fetch_translation_updates : CONFIG_INSTANCE.fetch_translation_updates;
    }

    public static boolean isConfigLoaded() {
        return CONFIG_INSTANCE != null;
    }

    public static void init() { }

    private FeatureControl() { }
}
