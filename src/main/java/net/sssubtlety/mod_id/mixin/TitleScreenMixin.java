package net.sssubtlety.mod_id.mixin;

import net.sssubtlety.mod_id.Init;
import net.minecraft.client.gui.screen.TitleScreen;
import net.sssubtlety.mod_id.ModId;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(TitleScreen.class)
public class TitleScreenMixin {
	@Inject(method = "init", at = @At("TAIL"))
	public void exampleMod$onInit(CallbackInfo ci) {
		ModId.LOGGER.info("This line is printed by an example mod mixin!");
	}
}
